from niomic.views import StaticView
from niomic.models import Dreamreal
from django.views.generic import TemplateView
from django.urls import path
from django.views.generic import ListView
from . import views

urlpatterns = [
    path('static/', StaticView.as_view()),
    path('dreamreals/', ListView.as_view(
        template_name='niomic/dreamreal_list.html',
        model=Dreamreal,
        context_object_name="dreamreals_objects")),
    path('', TemplateView.as_view(template_name='login.html')),
    path('login/', views.login, name="login")
]
